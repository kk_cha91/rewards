class Reward
	def self.get_reward file_path
		h = Hash.new(0)
		read_file = File.readlines(file_path)
		read_file.each_with_index do |line, i|
			if line.include?('accepts')
				line_data = line.split('accepts')
				c = line_data.first.strip[-1,1]
				Reward.check_data(read_file[0..i], h, c, 1.0)
			end	
		end
		return h
	end

	def self.check_data arr, h, c, count
		return if arr.empty?	
		arr.each_with_index do |ar, i|
			line_data = ar.split('recommends')
			k = line_data.first.strip[-1, 1]
			if ar.include?("recommends #{c}")
				h[k]+=count
				count/=2
				Reward.check_data(arr[0..i], h, k, count)
				break
			end
		end
	end
	puts Reward.get_reward('/home/komalpc/Desktop/input_reward.rb')
end